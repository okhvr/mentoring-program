import { subtract } from './subtract';

test('substract 4 -1 to equal 3', () => {
    expect(subtract(4, 1)).toBe(3);
});
