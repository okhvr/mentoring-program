import { multiplicate } from './multiplicate';

test('multiplicate 4 * 2 to equal 8', () => {
    expect(multiplicate(4, 2)).toBe(8);
});
