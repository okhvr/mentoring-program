import { Observable } from 'rxjs';

class MyObservable<T> extends Observable<T> {
    myPipe(...args: ((source: Observable<any>) => Observable<any>)[]): Observable<any> {
        return args.reduce((acc, cur) => cur(acc), this);
    }
}

export function creationOperator(): MyObservable<string> {
    return new MyObservable((subscriber) => {
        const timer = setInterval(() => subscriber.next(getRandomSymbol()), 1000);
        return () => clearInterval(timer);
    });
}

function getRandomSymbol(): string {
    const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    return alphabet[Math.floor(Math.random() * alphabet.length)];
}
