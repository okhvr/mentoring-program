import { Subscriber, Subscription } from 'rxjs';

export class AwesomeSubject<T> {
    constructor(private skip: number) {}
    private observers: {observer: Subscriber<T>, skip: number}[] = [];

    next(num: T) {
        this.observers.forEach((o) => {
            if (o.skip <= 0) {
                o.observer.next(num);
            } else {
                o.skip -= 1;
            }
        });
    }
    error(er) { this.observers.forEach(o => o.observer.error(er)); }

    complete() { this.observers.forEach(o => o.observer.complete()); }

    subscribe(next: ((val: T) => void),
              error?: (error: any) => void,
              complete?: () => void): Subscription {
        const observer = toObserver(next, error, complete);
        this.observers.push({ observer, skip: this.skip });
        return new Subscription(
            () => { this.observers = this.observers.filter(o => o.observer !== observer); });
    }
}

function toObserver<T>(
    observer: ((val: T) => void),
    error?: (error: any) => void,
    complete?: () => void): Subscriber<T> {
    return new Subscriber(observer, error, complete);
}
