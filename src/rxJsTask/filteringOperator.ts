import { Observable } from 'rxjs';

export function filteringOperator(word: string) {
    return (source: Observable<string>): Observable<string> => {
        return new Observable((observer) => {
            return source.subscribe({
                next(x) {
                    if (word.toUpperCase().includes(x.toUpperCase())) {
                        observer.next(x);
                    }
                },
                error(err) { observer.error(err); },
                complete() { observer.complete(); },
            });
        });
    };
}
