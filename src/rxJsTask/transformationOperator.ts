import { Observable } from 'rxjs';

export function transformationOperator() {
    return (source: Observable<string>) => {
        return new Observable((observer) => {
            return source.subscribe({
                next(x) { observer.next(switchCase(x)); },
                error(err) { observer.error(err); },
                complete() { observer.complete(); },
            });
        });
    };
}

function switchCase(s: string): string {
    return s === s.toLowerCase() ? s.toUpperCase() : s.toLowerCase();
}
