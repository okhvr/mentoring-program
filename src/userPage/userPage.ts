import { greeter, draw } from '../shared/index';
import { User } from './userClass';

const sendUserInfoButton = document.getElementById('sendUserInfo');
sendUserInfoButton.addEventListener('click', speakWithSanta);

function speakWithSanta(evt: Event) {
    evt.preventDefault();
    const inputsValues = [].map.call(
        document.getElementsByTagName('input'),
        (el: HTMLInputElement) => el.value);

    const [name, surName, age, present] = inputsValues;

    const user = new User(name, surName, age, present);
    const chat = document.getElementById('chat');
    draw(chat, user.introduce());
    draw(chat, greeter(user));
    draw(chat, 'How old are you?');
    draw(chat, user.sayAge());
    draw(chat, 'What do you wish?');
    draw(chat, user.orderPresent());
    draw(chat, 'Will be done!');
}
