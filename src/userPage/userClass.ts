export class User {
    fullName: string;

    constructor(public firstName: string,
                public lastName: string,
                private age: string,
                private present: string) {
        this.fullName = `${firstName} ${lastName}`;
    }

    introduce(): string {
        return `Hi! My name is ${this.fullName}.`;
    }

    sayAge(): string {
        return ` I'm ${this.age}`;
    }

    orderPresent(): string {
        return `I would like to receive ${this.present}`;
    }
}
