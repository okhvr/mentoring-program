export function draw(domNode: HTMLElement, message: string) {
    const newNode = document.createElement('p');
    newNode.innerHTML = message;
    domNode.appendChild(newNode);
}
