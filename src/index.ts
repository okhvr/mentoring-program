import { showName } from './task1/showName';
import './styles/main.scss';
import { creationOperator } from './rxJsTask/creationOperator';
import { filteringOperator } from './rxJsTask/filteringOperator';
import { transformationOperator } from './rxJsTask/transformationOperator';
import { tap } from 'rxjs/operators';
import { AwesomeSubject } from './rxJsTask/AwesomeSubject';

const button = document.getElementById('button');
button.classList.add('button');
button.addEventListener('click', showName);

const stream = creationOperator().myPipe(
    filteringOperator('OLHA'),
    tap(l => console.log('After filter:', l)),
    transformationOperator(),
).subscribe(l => console.log(l));

const sub = new AwesomeSubject(2);
const subscription1 = sub.subscribe(i => console.log('Sybscriber1: ', i));
sub.next(1);
sub.next(2);
sub.next(3);
sub.next(4);
sub.subscribe(
    i => console.log('Sybscriber2: ', i),
    er => console.error(er),
    () => console.log('COMPLETE'));
sub.next(5);
sub.next(6);
sub.next(7);
subscription1.unsubscribe();
sub.next(8);
sub.next(9);
sub.complete();
