const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const { CheckerPlugin } = require('awesome-typescript-loader');

const path = require('path');
const isDevelopment = process.env.NODE_ENV !== 'production';

module.exports = {
    entry: {
        main: './src/index.ts',
        adminPage: './src/adminPage/adminPage.ts',
        userPage: './src/userPage/userPage.ts'
    },
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    devtool: isDevelopment ? 'inline-source-map' : 'source-map',
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    use: [
                        {
                            loader: 'css-loader', // translates CSS into CommonJS
                            options: { sourceMap: isDevelopment }
                        },
                        'postcss-loader',
                        {
                            loader: 'sass-loader', // compiles Sass to CSS, using Node Sass by default
                            options: { sourceMap: isDevelopment }
                        }
                    ]
                })

            },
            {
                test: /\.ts?$/,
                use: 'awesome-typescript-loader'
            },
            {
                test: /\.ts$/,
                enforce: 'pre',
                use: [
                    {
                        loader: 'tslint-loader',
                        options: {emitErrors: true},
                    }
                ]
            }
        ]
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    },
    plugins: [
        new CleanWebpackPlugin(['dist']),
        new ExtractTextPlugin('styles.css'),
        new CheckerPlugin(),
        new HtmlWebpackPlugin({
            inject: true,
            chunks: ['main'],
            filename: 'index.html',
            template: 'index.html'
        }),
        new HtmlWebpackPlugin({
            inject: true,
            chunks: ['adminPage'],
            filename: 'admin.html',
            template: 'src/adminPage/admin.html'
        }),
        new HtmlWebpackPlugin({
            inject: true,
            chunks: ['userPage'],
            filename: 'user.html',
            template: 'src/userPage/user.html'
        })
    ],
    optimization: {
        splitChunks: {
            cacheGroups: {
                commons: {
                    chunks: 'initial',
                    minChunks: 2,
                    minSize: 0 // This is example is too small to create commons chunks
                },
            }
        }
    },
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 3333
    }
};
